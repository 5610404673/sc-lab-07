package thear;

import java.util.ArrayList;

public class TheaterManagement {
	 double ticketPrices[][]; 
	 private Schedule s;
	 private ArrayList<Double> total  = new ArrayList<Double>();
	 
	 public TheaterManagement (){
		 s = new Schedule();
	 } 
		public void setMovie(String str) {
			s.setSchMovie(str);
		}
	 	public void addSeatsPrice(double amount){
	 		total.add(amount);
	 	}
	 	public double getSeatsPrice(){
	 		double ans =0;
	 		for(int x=0 ;x<total.size();x++){
	 			ans+=total.get(x);
	 		}
	 		return ans;
	 	}
	 	
	 	public void clear(){
	 		total.clear();
	 	}

		 public double getPrice(int i, int j){
			 return s.getPrice(i, j);
		 }
		 public void setSeat(int i, int j){
			s.setSeat(i, j);
		 }
		 public double[][] getSeat(){
			 return s.getSeat();
		 }
}
