package thear;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;



public class gui extends JFrame {
	JPanel panelseat,panelRight,panel3,panel4,panel5,panel6,panel7;
	JLabel seat[][],label1,label2,label3,label4,label5,label6;
	JTextField textrow,textcolumn;
	JTextArea ta;
	JButton button1,button2,button3,button4;
	JComboBox box,box2;
	ArrayList<String> list= new ArrayList<String>(); 
	ArrayList<String> list2= new ArrayList<String>(); 
	TheaterManagement con;
	String selectMovie;
	public gui(){
		creat();
	}
	public void creat(){
		con = new TheaterManagement();
		panelRight = new JPanel();
		panelseat = new JPanel();
		panel3 = new JPanel();
		panel4 = new JPanel();
		panel5 = new JPanel();
		panel6 = new JPanel();
		panel7 = new JPanel();
		label1 = new JLabel("Select Movie");
		label2 = new JLabel("Select Seat :");
		label3 = new JLabel("Select Price");
		label4 = new JLabel("Total Price: ");

		ta = new JTextArea();
		textrow = new JTextField(2);
		textcolumn = new JTextField(2);
		button1 = new JButton("add");
		button2 = new JButton("Show");
		button3 = new JButton("Show");
		button4 = new JButton("Submit");
		
		//combobox seat
		String[] movie = {"Batman:Dark Knight","Ironman 3","Titanic","Amazing Spiderman"};
		String[] price = {"all","10","20","30","40","50"};
		box = new JComboBox(movie);
		box2 = new JComboBox(price);
		selectMovie = (String) box.getSelectedItem();		
		setLayout(new BorderLayout());
		panelseat = new JPanel(new GridLayout(15,20));
		add(panelseat,BorderLayout.CENTER);
		add(panelRight,BorderLayout.EAST);
	
		//panel1 west seatmap
		seat = new JLabel[15][20]; // Allocating Size of Grid
		String str ="A B C D E F G H I L M N O P Q";
		String str2 ="a b c d e f g h i l m n o p q";
		StringTokenizer s = new StringTokenizer(str, " ");
	     while (s.hasMoreTokens()) {
	         list.add(s.nextToken());
	     }
		StringTokenizer s2 = new StringTokenizer(str2, " ");
		    while (s2.hasMoreTokens()) {
		         list2.add(s2.nextToken());
		     }
		for(int x = 0; x <15 ; x++){
			for(int y = 0; y < 20; y++){
				if (con.getPrice(x,y)==0){
					seat[x][y]=new JLabel();
					}
				else{		
					seat[x][y] = new JLabel(list.get(x) + (y+1),JLabel.CENTER);
					}
					seat[x][y].setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.")));
					panelseat.add(seat[x][y]);
				} 
	
			}
		//panel2 east information
		panelRight.setLayout(new BorderLayout());
		panelRight.add(panel3,BorderLayout.NORTH);
		panelRight.add(panel4,BorderLayout.CENTER);
		panelRight.add(button4,BorderLayout.SOUTH);
		
		panel3.setLayout(new BorderLayout(10,10));
		panel3.add(label1,BorderLayout.WEST);
		panel3.add(box,BorderLayout.CENTER);
		panel3.add(button2,BorderLayout.EAST);
		
		panel4.setLayout(new BorderLayout());
		panel4.add(panel5,BorderLayout.NORTH);
		panel4.add(ta,BorderLayout.CENTER); 
		panel4.add(panel6,BorderLayout.SOUTH);
		panel5.add(label2);
		panel5.add(textrow);
		panel5.add(textcolumn);
		panel5.add(button1);
		
		panel6.setLayout(new GridLayout(2,0,5,5));
		panel6.add(panel7);
		panel7.setLayout(new GridLayout(3,1,1,1));		
		panel6.add(label4);

		
		
		panel7.setLayout(new GridLayout(0,3,5,5));
		panel7.add(label3);
		panel7.add(box2);
		panel7.add(button3);

		button1.addActionListener(new ListenerMgr());
		button2.addActionListener(new ListenerMgr());
		button3.addActionListener(new ListenerMgr());
		button4.addActionListener(new ListenerMgr());
		
	
}
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
				if(e.getSource() == button1){
					String r =textrow.getText();
					String c =textcolumn.getText();	
					
					ta.append("\n Booking "+r+" "+c);			
					if(list2.indexOf(r) == -1){
					con.addSeatsPrice(con.getPrice(list.indexOf(r),Integer.parseInt(c)-1));
					con.setSeat(list.indexOf(r),Integer.parseInt(c)-1);}
					else{
						con.addSeatsPrice(con.getPrice(list2.indexOf(r),Integer.parseInt(c)-1));
						con.setSeat(list2.indexOf(r),Integer.parseInt(c)-1);
					}
				
					label4.setText("Total Price: "+con.getSeatsPrice());
				
				}
				if(e.getSource() == button2){
					if(((String) box.getSelectedItem()).equals(selectMovie) == false){
						con.setMovie((String)box.getSelectedItem());
						selectMovie =(String) box.getSelectedItem();
						panelseat.removeAll();
						for(int x = 0; x <15 ; x++){
							for(int y = 0; y < 20; y++){
								if (con.getPrice(x,y)==0){
									seat[x][y]=new JLabel();
									}
								else{		
									seat[x][y] = new JLabel(list.get(x) + (y+1),JLabel.CENTER);
									}
									seat[x][y].setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.")));
									panelseat.add(seat[x][y]);
								} 
					
							}

						ta.append("\n"+(String)box.getSelectedItem());
					}
				}
					if(e.getSource() == button3){
			
						for(int x = 0; x <15 ; x++){
							for(int y = 0; y < 20; y++){
								if (((String) box2.getSelectedItem()).equals("all")){
									seat[x][y].setVisible(true);
									}
								else{
									double select =Double.parseDouble((String) box2.getSelectedItem());
									if(select != con.getPrice(x, y)){
										seat[x][y].setVisible(false);}
														
									else{
										seat[x][y].setVisible(true);
										}
									}
								}				
							}				
						}
						if(e.getSource() == button4){
					
								JOptionPane.showMessageDialog(button4,"Thank you");
								con.clear();
								label4.setText("Total price : "+con.getSeatsPrice());
								ta.setText("");
								panelseat.removeAll();
								for(int x = 0; x <15 ; x++){
									for(int y = 0; y < 20; y++){
										if (con.getPrice(x,y)==0){
											seat[x][y]=new JLabel();
											}
										else{		
											seat[x][y] = new JLabel(list.get(x) + (y+1),JLabel.CENTER);
											}
											seat[x][y].setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.")));
											panelseat.add(seat[x][y]);
										} 
							
									}



						}
						
		}
	}
	}
	
